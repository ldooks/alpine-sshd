# Alpine SSHD

[![](https://badge.imagelayers.io/ldooks/alpine-sshd:latest.svg)](https://imagelayers.io/?images=ldooks/alpine-sshd:latest 'Get your own badge on imagelayers.io')

This is a small Alpine-based container using [openssh](https://pkgs.alpinelinux.org/package/v3.5/main/armhf/openssh).

The intended usage is to (temporary) access/modify shared volumes by SSH/SFTP.

## Usage

It takes 2 variables: `USER` and `PASSWORD`, both in cleartext. Redirect the TCP/22 port to whatever port you want.

    docker run -d \
    -p 2222:22 \
    -e USER=myusername \
    -e PASSWORD=mypassword \
    ldooks/alpine-sshd

Then you can use this container to SSH:

```
ssh -p 2222 user1@192.168.99.100
user1@192.168.99.100's password:
Connected to 192.168.99.100.
$> pwd
Remote working directory: /home/user1
$>
```

## Build

    $ make build
